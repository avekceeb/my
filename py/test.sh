#!/bin/bash

PYTHON=${PYTHON:-python3.8}

for f in avekceeb/tests/*.py ; do
  m=$(basename $f)
  $PYTHON -m unittest -v avekceeb.tests.${m%.py}
done

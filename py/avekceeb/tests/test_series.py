import unittest
import pathlib
import avekceeb.series as s


class TestSeries(unittest.TestCase):
    @staticmethod
    def getpath(*p):
        d = pathlib.Path(__file__).resolve().parent
        return pathlib.Path.joinpath(d, *p)

    def test_sanity(self):
        s1 = s.Series(TestSeries.getpath('fibonacci-small.csv'))
        self.assertIsNotNone(s1)

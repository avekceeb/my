import pathlib

import avekceeb.diybook as diy
from avekceeb.xslt import xslt
from avekceeb.tests import TestMy
import tempfile

test_data = '''
Hello!
This is wonderful paragraph.
OK? This is one more...
And the final one
'''


class TestDiy(TestMy):
    def test_diybook(self):
        book = diy.DiyBook()
        self.assertTrue(len(book.chapters) == 0)

    def test_render(self):
        pars = [diy.Paragraph(t) for t in test_data.split("\n") if t]
        chaps = [diy.Chapter(title='Sunshine Title', paragraphs=pars)]
        diy2fb2 = TestMy.get_xsl('diybook-to-fb2.xsl')
        with tempfile.NamedTemporaryFile(delete=True) as xml:
            xml_path = pathlib.Path(tempfile.gettempdir(), xml.name)
            diy.render(chapters=chaps, title='Le Pest', outfile=xml_path)
            # content = xml.read()
            fb2 = xslt(xml_path, diy2fb2)
        # print(xml_path)
        print(fb2)

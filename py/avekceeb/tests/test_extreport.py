import unittest
import pathlib
import avekceeb.extreport as e


class TestExtreport(unittest.TestCase):
    @staticmethod
    def getpath(*p):
        d = pathlib.Path(__file__).resolve().parent
        return pathlib.Path.joinpath(d, *p)

    def test_json(self):
        r1 = e.ExtReport.read_json(TestExtreport.getpath('extreport-1.json'))
        self.assertTrue(len(r1.tests) > 0)

    def test_csv(self):
        r1 = e.ExtReport.read_csv(TestExtreport.getpath('extreport-1.csv'))
        self.assertTrue(len(r1.tests) > 0)

    def test_compare(self):
        r1 = e.ExtReport()
        r1.tests.append(e.Test(name='A', status=True, result=10.0, sigma=1.2))
        r1.tests.append(e.Test(name='B', status=True, result=10.0, sigma=1.2))
        r1.tests.append(e.Test(name='C', status=True, result=10.0, sigma=1.2))
        r2 = e.ExtReport()
        r2.tests.append(e.Test(name='A', status=True, result=11.0, sigma=1.0))
        r2.tests.append(e.Test(name='B', status=True, result=9.0, sigma=0))
        r2.tests.append(e.Test(name='D', status=True, result=100.0, sigma=0.2))
        cmp, _, _, _, _ = e.ExtReport.comparison(r1, r2)
        self.assertTrue(len(cmp) > 0)
        r1 = e.ExtReport.read_json(TestExtreport.getpath('extreport-1.json'))
        r2 = e.ExtReport.read_json(TestExtreport.getpath('extreport-2.json'))
        cmp, missed, extra, new_fails, fixed = e.ExtReport.comparison(r1, r2)
        # for c in cmp:
        #     print(r1.format_comparison(c))
        self.assertTrue(len(cmp) > 0)
        self.assertTrue(len(missed) == 1)
        self.assertTrue(len(extra) == 1)
        self.assertTrue(len(new_fails) == 1)
        self.assertTrue(len(fixed) == 1)

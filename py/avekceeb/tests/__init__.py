import unittest
import pathlib
import random

sample_text = """
Plato considered geometry a condition of idealism concerned with universal truth.
In Republic, Socrates opposes the sophist Thrasymachus.
relativistic account of justice,
and argues that justice is mathematical in its conceptual structure,
and that ethics was therefore a precise and objective enterprise
with impartial standards for truth and correctness, like geometry.
The rigorous mathematical treatment Plato
gave to moral concepts set the tone for the western tradition
of moral objectivism that came after him.
His contrasting between objectivity and opinion became the basis
for philosophies intent on resolving the questions of reality,
truth, and existence.
He saw opinions as belonging to the shifting sphere
of sensibilities, as opposed to a fixed,
eternal and knowable incorporeality.
Where Plato distinguished between how we know things
and their ontological status, subjectivism
such as George Berkeley's depends on perception.
In Platonic terms, a criticism of subjectivism is that
it is difficult to distinguish between knowledge, opinions,
and subjective knowledge.
""" * 3


class TestMy(unittest.TestCase):
    @staticmethod
    def getpath(*p):
        d = pathlib.Path(__file__).resolve().parent
        return pathlib.Path.joinpath(d, *p)

    @staticmethod
    def get_xsl(xsl_name):
        d = pathlib.Path(__file__).resolve().parent
        return pathlib.Path.joinpath(d, '../../../xsl', xsl_name)

    @staticmethod
    def get_random_sentence():
        return random.choice(sample_text.split('.'))

    @staticmethod
    def get_random_word():
        return random.choice(sample_text.split()).strip(',. ')

    @staticmethod
    def get_random_text():
        return ' '.join([TestMy.get_random_sentence() for _ in range(random.randint(1, 5))])

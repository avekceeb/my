from avekceeb.tests import TestMy
from avekceeb.doc import Doc, Chapter, Paragraph


class TestDoc(TestMy):
    def test_doc(self):
        chapters = [
            Chapter(title=TestMy.get_random_word(),
                    paragraphs=[Paragraph(text=TestMy.get_random_text())
                                for _ in range(6)])
            for _ in range(3)]
        d = Doc(title=TestMy.get_random_word(), chapters=chapters)
        # print(d.xml())
        d.fb2(file='test.fb2')

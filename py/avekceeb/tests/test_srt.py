import unittest
import pathlib
import avekceeb.srt as srt


class TestSrt(unittest.TestCase):
    @staticmethod
    def getpath(*p):
        d = pathlib.Path(__file__).resolve().parent
        return pathlib.Path.joinpath(d, *p)

    def test_json(self):
        srt1 = srt.Srt(TestSrt.getpath('srt-1.txt'))
        self.assertTrue(len(srt1.soustitres) > 0)
        # for x in srt1.soustitres:
        #     print(x.text)

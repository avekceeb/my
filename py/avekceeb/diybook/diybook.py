#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""

   ::: DIY Book :::

Simple parser of markdown syntax
ONLY LIMITED SUBSET OF MARKUP SUPPORTED
plus some features introduced which
targeted to help writing non-technical texts

"""


import sys
import re
from .chapter import Chapter, Paragraph


if sys.version_info[0] < 3:
    raise Exception("Python 3 required")


class DiyBook(object):

    _ws_only_line_re = re.compile(r"^[ \t]*$", re.UNICODE)
    _chapter_re = re.compile(r"^(#+)[ \t]*(.+)[ \t]*$", re.UNICODE)
    # TODO: this is not neccesary, tabs should be replaced by single space?: 
    tab_width = 4
    # TODO:
    # fb2 tags:
    #     empty-line  strong emphasis  stanza / v
    # re.sub(r'(?P<name>[0-9])', 'x\g<name>x', '1 2 3 4 5')
    paragraph_replacements = [
            (r'{([^}]+)}',    r'<tag>\g<1></tag>'),
            (r'`+(.*)`+',     ''),  # TODO: code
            #(r'\[([^\]]*)\]', ''),  # TODO: instruction
            #(r'\/([^\/]*)\/', r'<variant>\g<1></variant>'), clashes with url //
            (r'\\([^\\]*)\\', r'<variant>\g<1></variant>'),
            (r'\*+([^\*]*)\*+',   r'<strong>\g<1></strong>'),
            (r'_+([^_]*)_+',     r'<emphasis>\g<1></emphasis>'),  # TODO: italics
            (r'\[([^\]]*)\]\(([^\)]*)\)',
                r'<link><link-text>\g<1></link-text><link-url>\g<2></link-url></link>'),
        ]

    def __init__(self, *files, **options):
        self.chapters = []
        self.current_ch = None
        self.current_par = ''
        self.options = options
        self.current_kind = None
        # TODO: options
        self.use_special_punctuation = True
        for f in files:
            if isinstance(f, (list, tuple)):
                self._parse_files(f)
            elif isinstance(f, str):
                self._parse_file(f)
            else:
                pass

    def __str__(self):
        t = ''
        for c in self.chapters:
            t += c.__str__() + "\n"
        return t

    # TODO: this crap should be static
    def _detab_line(self, line):
        if '\t' not in line:
            return line
        return re.sub('\t', ' ', line)

    #@staticmethod
    #def _wrap_into_tag(self, text, tag):
    #    return "<%s>%s</%s>" % (tag, text, tag)

    @staticmethod
    def _find_and_replace_in_line(text, regex, replacement):
        return re.sub(regex, replacement, text)

    @staticmethod
    def _strip_comments(text):
        if "<!--" not in text:
            return text
        start = 0
        while True:
            try:
                start_idx = text.index("<!--", start)
            except ValueError:
                break
            try:
                end_idx = text.index("-->", start_idx) + 3
            except ValueError:
                raise Exception(
                    "No closing comments around text %s" %
                    text[start_idx:start_idx+20])
            start = start_idx
            if start_idx < end_idx:
                text = text[:start_idx] + text[end_idx:]
        return text

    @staticmethod
    def _is_header(text):
        return text.startswith('#')

    @staticmethod
    def _is_preformat(text):
        return text.startswith(' ')

    #@staticmethod
    def _parse_paragraph(self, text):
        for r, s in self.paragraph_replacements:
            text = self._find_and_replace_in_line(text, r, s)
        return text

    def _is_break(self, text):
        return self._ws_only_line_re.search(text) is not None

    def _push_chapter(self):
        if self.current_ch is not None:
            if self.current_par:
                self.current_ch.paragraphs.append(Paragraph(self.current_par, kind=self.current_kind))
                self.current_par = ''
            self.chapters.append(self.current_ch)
            self.current_ch = None
        else:
            if self.current_par:
                ch = Chapter(paragraphs=[Paragraph(self.current_par, kind=self.current_kind)])
                self.chapters.append(ch)
                self.current_ch = None
                self.current_par = ''

    def _push_paragraph(self):
        if not self.current_par:
            return
        # TODO: in case of we pure text
        # this would spoil it with <tag>
        # ... so if self.text_mode or something...
        self.current_par = self._parse_paragraph(self.current_par)
        # TODO: if (we really want it)
        self.current_par += "\n\n"
        if self.current_ch is not None:
            self.current_ch.paragraphs.append(Paragraph(self.current_par, kind=self.current_kind))
            self.current_par = ''
        else:
            self.current_ch = Chapter(paragraphs=[Paragraph(self.current_par, kind=self.current_kind)])
            self.current_par = ''

    def _append_to_paragraph(self, text):
        joiner = "\n" if self.current_kind == 'pre' else ' '
        if self.use_special_punctuation:
            # text = text.replace("---", "&#8212;")
            text = text.replace("--", "&#8211;")
            # text = text.replace("...", "&#8230;")
            # text = text.replace(" . . . ", "&#8230;")
            # text = text.replace(". . .", "&#8230;")
        if self.current_par:
            self.current_par += joiner + text
        else:
            self.current_par += text

    def _start_header(self, text):
        m = self._chapter_re.search(text)
        # TODO: if m is None
        self._push_chapter()
        self.current_ch = Chapter(title=m.group(2))

    def _parse_md(self, text):
        if not isinstance(text, str):
            text = str(text, 'utf-8')
        text = text.replace("\r\n", "\n")
        text = text.replace("\r", "\n")
        text = self._strip_comments(text)
        text = self._ws_only_line_re.sub("", text)
        lines = text.splitlines()
        for line in lines:
            line = self._detab_line(line)
            if self._is_header(line):
                # TODO: header extra attributes
                self._start_header(line)
                self.current_kind = None 
            elif self._is_break(line):
                self._push_paragraph()
                self.current_kind = None 
            elif self._is_preformat(line):
                if self.current_kind != 'pre': 
                    self._push_paragraph()
                self.current_kind = 'pre' 
                self._append_to_paragraph(line.strip())
            else:
                self.current_kind = 'common' 
                self._append_to_paragraph(line.strip())
        self._push_chapter()
        # TODO: markup inside paragraphs

    def _parse_file(self, fl):
        # TODO: save filename and some metadata
        # TODO: check if exists
        with open(fl, 'r') as f:
            self._parse_md(f.read())

    def _parse_files(self, files):
        for f in files:
            self._parse_file(f)


#!/usr/bin/python3
# -*- coding: utf-8 -*-

class Paragraph(object):
    
    def __init__(self, text, kind=None):
        self.content = text
        self.kind = 'common' if kind is None else kind

    def __str__(self):
        return self.content


class Chapter(object):

    def __init__(self, title='***', paragraphs=None):
        self.title = title
        self.paragraphs = [] if paragraphs is None else paragraphs

    def __str__(self):
        t = ''
        for p in self.paragraphs:
            t += "--->" + p.__str__() + "\n"
        return self.title + "\n" + t



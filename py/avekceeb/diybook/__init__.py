
from .diybook import DiyBook 
from .renderxml import render
from .chapter import Paragraph, Chapter


def diy2xml(*files, **options):
    book = DiyBook(*files, **options)
    # print("%s" % md)
    filex = options['outfile'] if options else None
    caption = options['title'] if options else 'no title'
    render(chapters=book.chapters, outfile=filex, title=caption)


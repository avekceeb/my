#!/usr/bin/python3
# -*- coding: utf-8 -*-

# https://docs.python.org/3/library/xml.etree.elementtree.html
# https://pymotw.com/2/xml/etree/ElementTree/create.html


# from .chapter import Chapter
import xml.etree.ElementTree as et
from sys import exit

xml_template = """<?xml version="1.0" encoding="UTF-8"?>
<DIY-book>
<description>
    <title-info>
        <genre>literature_classics</genre>
        <author>
            <first-name>Имя</first-name>
            <middle-name>Отчество</middle-name>
            <last-name>Фамилия</last-name>
        </author>
        <book-title>Тут должно быть название</book-title>
        <lang>ru</lang>
    </title-info>
    <document-info>
        <author>
            <nickname>MarkDown Renderer</nickname>
        </author>
        <date value="2002-10-15">15 ноября 2002г., 19:53</date>
        <id>123456789</id>
        <version>0.0</version>
    </document-info>
</description>
<body>
    <title>
        <p>Тут должно быть название</p>
    </title>
</body>
</DIY-book>
"""


def find_first(root, what):
    g = root.findall(what)
    assert len(g) > 0
    return g[0]


def set_title(root, title):
    find_first(root, 'description/title-info/book-title').text = title


def add_section(parent, title):
    s = et.SubElement(parent, 'section')
    t = et.SubElement(s, 'title')
    tp = et.SubElement(t, 'p')
    tp.text = title
    return s


def add_paragraph(parent, paragraph):
    # We are expecting mixed content
    # in paragraph (inline formatting <strong> etc
    # so lets parse it in this way:
    if paragraph.kind != 'pre':
        x = ''
        try:
            x = et.fromstring("<p>" + paragraph.content + "</p>")
        except Exception:
            print("Error while parsing:\n%s" % paragraph.content)
            exit(1)
        parent.append(x)
    else:
        pre = et.SubElement(parent, 'pre')
        for line in paragraph.content.split('\n'):
            if not line.strip():
                # TODO: new stanza
                continue
            ln = et.SubElement(pre, 'line')
            ln.text = line


def render(chapters=[], title='unknown', outfile=None):
    root = et.fromstring(xml_template)
    root.set('xmlns:l', 'http://www.w3.org/1999/xlink')
    # root.set('xmlns', 'http://www.gribuser.ru/xml/fictionbook/2.0')
    set_title(root, title)
    body = find_first(root, 'body')
    for c in chapters:
        s = add_section(body, c.title)
        for pg in c.paragraphs:
            add_paragraph(s, pg)
    if outfile is not None:
        et.ElementTree(root).write(outfile, encoding="UTF-8", xml_declaration=True)
        # with open(outfile, 'w') as f:
        #    f.write(et.tostring(root, encoding='unicode', method='text'))
    else:
        print(et.tostring(root, encoding='unicode', method='xml'))

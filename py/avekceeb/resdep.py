import json


class Jsonable:
    def js(self):
        return json.dumps(
            self,
            default=lambda x: {
                k: v for k, v in x.__dict__.items() if v is not None},
            sort_keys=False,
            indent=4)


class RunReport(Jsonable):
    def __init__(self, run, results=[]):
        self.run = run
        self.results = results


class Run(Jsonable):
    def __init__(self, n, v=None, ts=None, tags=[], i=None,
                 total=0, fails=0, skips=0, time=None):
        self.id = i
        self.name = n
        self.version = v
        self.ts = ts
        self.time = time
        self.tests = total
        self.failures = fails
        self.skipped = skips
        self.tags = tags
        self.meta = {}


class TestResult(Jsonable):
    def __init__(self, t, r):
        self.test = t
        self.result = r


class Test(Jsonable):
    def __init__(self, ns, name, i=None):
        self.ns = ns
        self.name = name


class Result(Jsonable):
    def __init__(self, s, d, score=None, unit=None):
        self.status = s
        self.time = d
        self.score = score
        self.unit = unit


class Project(Jsonable):
    def __init__(self, n, d='', i=None):
        self.id = i
        self.name = n
        self.description = d


class Product(Jsonable):
    def __init__(self, n, d='', i=0, pid=None):
        self.id = i
        self.name = n
        self.description = d
        self.project_id = pid


class Version(Jsonable):
    def __init__(self, n, p, i=None):
        self.id = i
        self.name = n
        self.product = p

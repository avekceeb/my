import json
import csv
import pathlib
import sys
from statistics import geometric_mean
from datetime import datetime
from pathlib import Path
import avekceeb.console as console


class Test:
    def __init__(self, name='Unknown', component='', status=False, result=None, sigma=None):
        self.name = name
        self.component = component
        self.status = status
        self.result = result
        self.sigma = sigma

    def __eq__(self, x):
        return self.name == x.name

    def __lt__(self, x):
        if self.sigma is not None and \
                self.result and \
                x.sigma is not None and \
                x.result:
            return (self.sigma/self.result) < (x.sigma/x.result)
        else:
            return self.name < x.name

    def __str__(self):
        return f"{self.name} {self.result:.0f} \u00B1{self.sigma:.0f}"


# TODO: calculate iterations
# and store % of deviation
# Store meta info from 'run'
# check if there are duplicate names

class ExtReport:
    date_in_format = '%Y-%m-%dT%H:%M:%S.00000%z'
    date_out_format = '%d %B %H:%M'

    def __init__(self, name='', file_name=''):
        self.tests = []
        self.name_length = 0
        self.comp_length = 0
        self.date = 'Unknown'
        self.duration = 'Unknown'
        self.duration_seconds = 0
        self.ts = 'Unknown'
        self.file_name = Path(file_name).stem
        self.name = name
        self.product_version = ''
        self.meta = {}

    def __str__(self):
        return '\n'.join([self.format_test(t) for t in self.tests])

    def __iter__(self):
        return iter(self.tests)

    def get_name(self):
        n = self.name if self.name else self.file_name
        n = n if n else 'Unknown'
        return n

    @staticmethod
    def read_json(jsonpath):
        r = ExtReport(file_name=jsonpath)
        with open(jsonpath) as f:
            try:
                report = json.loads(f.read())
            except Exception as e:
                console.red(f'Error loading {jsonpath}: {e}')
                return r
        m = report.get('machine')
        if m:
            r.name = m.get('name')
        r.meta.update(report['run'])
        try:
            r.ts = report['run']['start_time']  # for sql
            t1 = datetime.strptime(report['run']['start_time'], ExtReport.date_in_format)
            t2 = datetime.strptime(report['run']['end_time'], ExtReport.date_in_format)
            delta = (t2 - t1).seconds
            hours, remainder = divmod(delta, 3600)
            minutes, _ = divmod(remainder, 60)
            r.duration = f'{hours}h {minutes}m'
            r.date = t1.strftime(ExtReport.date_out_format)
            r.duration_seconds = delta
        except Exception as e:
            console.orange(f'Error parsing date: {e}')
        # Compose Prod Version
        job_url = report['run'].get('job_url', '')
        job_id = ''
        if job_url:
            job_id = [x for x in job_url.split('/') if x][-1]
        fw = report['run']['framework_version'][:7]
        mr = report['run'].get("merge_request", "???")
        xy = report['run']["panda_commit_hash"][:7]
        if job_id:
            r.product_version += f'job-{job_id}'
        if fw and fw != '???':
            r.product_version += f'fw-{fw}'
        if mr and mr != '???':
            r.product_version += f'-MR{mr}'
        if xy and xy != '???':
            r.product_version += f'-XY-{xy}'
        for t in report['tests']:
            name = t['name']
            comp = t['component']
            if len(name) > r.name_length:
                r.name_length = len(name)
            if comp and len(comp) > r.comp_length:
                r.comp_length = len(comp)
            if t['compile_status'] != 0 or t['execution_status'] != 0:
                r.tests.append(Test(name, comp, False))
                continue
            execs = t.get('execution_forks')
            if execs is None or len(execs) == 0:
                r.tests.append(Test(name, comp, False))
                continue
            iters = execs[0].get('iterations')
            if iters is None or len(iters) == 0:
                r.tests.append(Test(name, comp, False))
                continue
            sigma = t.get('stdev_time') \
                if len(iters) > 1 and t.get('stdev_time') is not None \
                else 0.0
            # execs[0].get('avg_time') ???
            r.tests.append(Test(name, comp, True, t.get('mean_time'), sigma))
        r.tests = sorted(r.tests)
        return r

    def format_test(self, t):
        if not t.status:
            return f'{t.name.ljust(self.name_length)} FAILED'
        rel_sigma = (t.sigma/t.result)*100
        return f'{t.component.ljust(self.comp_length)} ' +\
               f'{t.name.ljust(self.name_length)} ' +\
               f'{t.result:10.0f} \u00B1{rel_sigma:2.0f}%'

    def format_comparison(self, c):
        # TODO: check if status differs
        # {'n': t1.name, 't1': t1, 't2': t2, 'd': 1.0 - t1.result / t2.result}
        return f'{c["t1"].name.ljust(self.name_length)} ' +\
               f'{c["t1"].result:12.0f} ' +\
               f'{c["t2"].result:12.0f} ' +\
               f'{c["d"]:2.0f}%'

    def geomean(self):
        if not self.tests:
            return 0
        res = [t.result for t in self.tests if t.status]
        if not res:
            return 0
        return geometric_mean(res)

    @staticmethod
    def read_csv(csvpath):
        """
        Benchmark name,Time ns/op,Ext Time sec,RSS max KB,Compile sec,Size bytes,PN Hash
        AccessBinaryTrees_test.java,2675354.4448876665,8.29732,95188,0.94,33124,N/A
        """
        r = ExtReport(file_name=csvpath)
        with open(csvpath, newline='') as f:
            rows = [r for r in csv.reader(f, delimiter=',')]
            cols = rows.pop(0)
        for row in rows:
            if len(row) < 7:
                console.warn(f'Too short row: {"|".join(row)}')
                continue
            name = '.'.join(row[0].split('.')[:-1])
            comp = ''
            if len(name) > r.name_length:
                r.name_length = len(name)
            try:
                # drop all after dot .***
                result = float(row[1].split('.')[0])
            except ValueError:
                r.tests.append(Test(name, comp, False))
                continue
            if result is None or result <= 0:
                r.tests.append(Test(name, comp, False))
                continue
            # sigma = 0 because no iteration info
            r.tests.append(Test(name, comp, True, result, 0))
        r.tests = sorted(r.tests)
        return r

    @staticmethod
    def comparison(r1, r2):
        comparison = []
        fixed = []
        new_fails = []
        # intersection of tests in both reports:
        for t1 in (t for t in r1 if t in r2):
            t2 = r2.tests[r2.tests.index(t1)]
            # TODO: check if status differs
            if t1.status != t2.status:
                if t2.status:
                    fixed.append(t1.name)
                else:
                    new_fails.append(t1.name)
            if t1.result and t2.result:
                comparison.append(
                    {'n': t1.name, 't1': t1, 't2': t2, 'd': (1.0-t1.result/t2.result)*100})
            else:
                pass  # ???
        comparison.sort(key=lambda d: d['d'], reverse=True)
        fixed.sort()
        new_fails.sort()
        # missed in 2-nd report:
        missed = [t for t in r1 if t not in r2]
        # missed in 1-st report:
        extra = [t for t in r2 if t not in r1]
        return comparison, missed, extra, new_fails, fixed

    @staticmethod
    def guess_by_extension(f):
        if f.endswith('.json'):
            return ExtReport.read_json(f)
        elif f.endswith('.csv'):
            return ExtReport.read_csv(f)
        else:
            console.red(f'Unknown file extension: {f}')
            sys.exit(1)


def console_report(reports, full=False, brief=False):
    for f in reports:
        r = ExtReport.guess_by_extension(f)
        console.caption_info(r.get_name())
        console.info(pathlib.Path(f).stem)
        print(f'tests   : {len(r.tests)}')
        failed = len([t for t in r.tests if not t.status])
        if failed > 0:
            console.red(f'failed  : {failed}')
        else:
            console.ok('failed  : 0')
        gm = r.geomean()
        if gm:
            print(f'geomean : {gm:.2f}')
        else:
            console.warn('geomean : N/A')
        print(f'started : {r.date}')
        print(f'duration: {r.duration}')
        if not full:
            print()
            continue
        # if failed > 0:
        #     console.caption_red('Failed tests:')
        #     for x in r.tests:
        #         if not x.status:
        #             console.red(r.format_test(x))
        # console.caption_warn('Most deviative tests:')
        # print('\n'.join(
        #     [r.format_test(x) for x in r.tests if x.status and x.sigma][-3:]))
        # if full:
        console.caption_info('All tests:')
        print(r)
        print()


def compare_reports(f1, f2, full=False):
    r1 = ExtReport.guess_by_extension(f1)
    r2 = ExtReport.guess_by_extension(f2)
    g1 = r1.geomean()
    if g1 > 0:
        ratio = r2.geomean()/g1
        if ratio < 1:
            console.caption_green(f'Geomean Ratio: {ratio:.2f}')
        else:
            console.caption_yellow(f'Geomean Ratio: {ratio:.2f}')
    cmp, missed, extra, new_fails, fixed = ExtReport.comparison(r1, r2)
    gm_passed_1 = geometric_mean([t['t1'].result for t in cmp])
    gm_passed_2 = geometric_mean([t['t2'].result for t in cmp])
    console.info(f'GM(only both): {gm_passed_1:.2f} vs {gm_passed_2:.2f} : {gm_passed_2/gm_passed_1:.2f}')
    for r, f in ((r1, f1), (r2, f2)):
        console.caption_info(r.get_name())
        print(f'tests   : {len(r.tests)}')
        failed = len([t for t in r.tests if not t.status])
        if failed > 0:
            console.red(f'failed  : {failed}')
        else:
            console.ok('failed  : 0')
        print(f'geomean : {r.geomean():.2f}')
        print(f'duration: {r.duration}')
    better = [i for i in cmp if i['d'] < 0]
    worse = [i for i in cmp if i['d'] > 0]
    if not full:
        better = better[-3:]
        worse = worse[:3]
    if len(worse) > 0:
        console.caption_orange('Worse:')
        for c in worse:
            console.orange(r1.format_comparison(c))
    if len(better) > 0:
        console.caption_green('Better:')
        for c in better:
            console.green(r1.format_comparison(c))
    if len(missed) > 0:
        console.caption_red('Missed tests:')
        for t in missed:
            console.red(r1.format_test(t))
    if len(extra) > 0:
        console.caption_green('New tests:')
        for t in extra:
            console.green(r2.format_test(t))
    if len(new_fails) > 0:
        console.caption_red('New fails:')
        for n in new_fails:
            console.red(n)
    if len(fixed) > 0:
        console.caption_green('Fixed tests:')
        for n in fixed:
            console.green(n)


if __name__ == '__main__':
    console_report(sys.argv[1:])

import csv
from os.path import basename


class SeriesException(Exception):
    pass


class Series:
    """
    TODO:
        force column types
        filter values like 'N/A', None etc
        export csv, json
        iterator, index, dict
    """
    def __init__(self, data=None, column_names=None, name=None):
        self.name = name
        self.data = data if data else []
        self.cols = column_names if column_names else []

    def get_col_index(self, name):
        if not self.cols:
            raise SeriesException(f'No column names for this data')
        try:
            return self.cols.index(name)
        except ValueError as _:
            raise SeriesException(f'No such column: {name}')

    def get_by_name(self, name):
        i = self.get_col_index(name)
        return [d[i] for d in self.data]

    # def get_by_col(self, col):
    #     check type of col...

    def get_dict(self, column):
        if isinstance(column, str):
            column = self.get_col_index(column)
        d = dict()
        for row in self.data:
            # we can check if there are repetitions in keys...
            # should we?
            key = row.pop(column)
            d[key] = row
        return d

    @staticmethod
    def read_csv(csv_path, have_header=False, delimiter=','):
        with open(csv_path, newline='') as f:
            rows = [r for r in csv.reader(f, delimiter=delimiter)]
            cols = rows.pop(0) if have_header else None
        return Series(data=rows, column_names=cols, name=basename(csv_path))


if __name__ == '__main__':
    s = Series.read_csv('test-data-000.csv', have_header=True)
    print(s.cols)

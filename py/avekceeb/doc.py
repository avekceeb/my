import datetime
import pathlib
import tempfile
import xml.etree.ElementTree as et
from avekceeb.xslt import xslt


class Paragraph:
    def __init__(self, text, style=None):
        self.content = text
        self.style = style


class Chapter:
    def __init__(self, title='', paragraphs=[]):
        self.title = title
        self.paragraphs = paragraphs


class Meta:
    def __init__(self, title='', author_f='', author_m='', author_l='', lang='en'):
        self.title = title
        self.genre = 'fiction'
        self.author_first_name = author_f
        self.author_middle_name = author_m
        self.autor_last_name = author_l
        self.lang = lang
        self.id = '12345678'
        self.version = '0'
        self.date = datetime.datetime.utcnow()

    def __getattr__(self, name):
        if 'short_date' == name:
            return '2020-01-01'
        elif 'long_date' == name:
            return '2022 Jan 1'
        return ''


class Doc:
    template = """<?xml version="1.0" encoding="UTF-8"?>
    <DIY-book>
    <description>
        <title-info>
            <genre>{meta.genre}</genre>
            <author>
                <first-name>{meta.author_first_name}</first-name>
                <middle-name>{meta.author_middle_name}</middle-name>
                <last-name>{meta.autor_last_name}</last-name>
            </author>
            <book-title>{meta.title}</book-title>
            <lang>{meta.lang}</lang>
        </title-info>
        <document-info>
            <author>
                <nickname>Some Guy</nickname>
            </author>
            <date value="{meta.short_date}">{meta.long_date}</date>
            <id>{meta.id}</id>
            <version>{meta.version}</version>
        </document-info>
    </description>
    <body>
        <title>
            <p>{meta.title}</p>
        </title>
    </body>
    </DIY-book>
    """

    def __init__(self, title=None, meta=None, chapters=[]):
        self.meta = meta if meta else Meta()
        if title:
            self.meta.title = title
        self.chapters = chapters

    def xml(self, file=None):
        root = et.fromstring(Doc.template.format(meta=self.meta))
        root.set('xmlns:l', 'http://www.w3.org/1999/xlink')
        body = Doc.find_first(root, 'body')
        if body is None:
            return
        for c in self.chapters:
            s = Doc.add_section(body, c.title)
            for pg in c.paragraphs:
                Doc.add_paragraph(s, pg)
        if file is not None:
            et.ElementTree(root).write(file, encoding="UTF-8", xml_declaration=True)
        else:
            return et.tostring(root, encoding='unicode', method='xml')

    def fb2(self, file=None):
        d = pathlib.Path(__file__).resolve().parent
        xsl = pathlib.Path.joinpath(d, '../../xsl/diybook-to-fb2.xsl')
        with tempfile.NamedTemporaryFile(delete=True) as xml:
            xml_path = pathlib.Path(tempfile.gettempdir(), xml.name)
            self.xml(file=xml_path)
            fb2 = xslt(xml_path, xsl)
        if file:
            with open(file, 'w', encoding='UTF-8') as f:
                f.write(fb2)
        else:
            return fb2

    @staticmethod
    def find_first(root, what):
        g = root.findall(what)
        return g[0] if len(g) > 0 else None

    @staticmethod
    def add_section(parent, title):
        s = et.SubElement(parent, 'section')
        t = et.SubElement(s, 'title')
        tp = et.SubElement(t, 'p')
        tp.text = title
        return s

    @staticmethod
    def add_paragraph(parent, paragraph):
        # We are expecting mixed content
        # in paragraph (inline formatting <strong> etc
        # so lets parse it in this way:
        parent.append(et.fromstring("<p>" + paragraph.content + "</p>"))

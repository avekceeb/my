from re import compile as recompile
from avekceeb.doc import Doc, Paragraph, Chapter


class SrtEntry:
    def __init__(self, txt, start, end):
        self.text = txt
        self.start = start
        self.end = end


class Srt:
    time_interval_re = recompile(
        r'^(?P<H1>\d\d):'
        r'(?P<M1>\d\d):'
        r'(?P<S1>\d\d),'
        r'(?P<ms1>\d\d\d)'
        r' +--> +'
        r'(?P<H2>\d\d):'
        r'(?P<M2>\d\d):'
        r'(?P<S2>\d\d),'
        r'(?P<ms2>\d\d\d)$')

    def __init__(self, filepath):
        self.soustitres = list()
        with open(filepath, 'r') as f:
            line = f.readline()
            while line:
                start, end = Srt.parse_time_interval(line)
                if start is not None:
                    text = ''
                    line_text = f.readline()
                    while line_text and line_text.strip():
                        text = ' '.join((text, line_text.strip()))
                        line_text = f.readline()
                    self.soustitres.append(SrtEntry(text.strip(), start, end))
                line = f.readline()

    def to_doc(self, title=''):
        # assuming titres are in the coorect order
        # adn there is no need to sort them by time
        chapters = []
        paragraphs = []
        for s in self.soustitres:
            if len(paragraphs) == 0:
                ch_title = Srt.ms_to_ts(s.start)
            paragraphs.append(Paragraph(text=s.text))
            if len(paragraphs) > 40:
                chapters.append(Chapter(title=ch_title, paragraphs=paragraphs))
                paragraphs = []
        if len(paragraphs) > 0:
            chapters.append(Chapter(title=ch_title, paragraphs=paragraphs))
        return Doc(title=title, chapters=chapters)

    @staticmethod
    def ms_to_ts(ms):
        ms //= 1000
        h = ms // 3600
        ms %= 3600
        m = ms // 60
        ms %= 60
        return '{:02d}:{:02d}:{:02d}'.format(h, m, ms)

    @staticmethod
    def parse_time_interval(line):
        m = Srt.time_interval_re.match(line)
        if not m:
            return None, None
        d = m.groupdict()
        return (
            int(d['ms1']) +
            int(d['S1']) * 1000 +
            int(d['M1']) * 60000 +
            int(d['H1']) * 3600000,
            int(d['ms2']) +
            int(d['S2']) * 1000 +
            int(d['M2']) * 60000 +
            int(d['H2']) * 3600000
        )

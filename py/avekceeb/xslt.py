
from lxml import etree


def xslt(xml, xsl, params={}):
    xml_tree = etree.parse(str(xml))
    xslt_tree = etree.parse(str(xsl))
    transform = etree.XSLT(xslt_tree)
    return str(transform(xml_tree, **params))

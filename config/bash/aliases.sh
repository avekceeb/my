##################################################
# add to profile
##################################################
# my=$HOME/d/my
#  [[ -d $my/py ]] && {
#     export PYTHONPATH=$my/py/:$PYTHONPATH
# }
# [[ -d $my/bin ]] && {
#     export PATH=$my/bin/:$PATH
# }
# [[ -d $my/config/bash ]] && {
#     for f in $my/config/bash/* ; do
#         echo "sourcing $f"
#         source $f
#     done
# }
# unset my
##################################################
alias xxh='ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null'
alias xcp='scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null'
alias hist="history | sed -e 's/^.......//'"
#alias fake='ssh -i ~/id_rsa -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null root@x100.fake.net'
#alias faket="ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ~/id_rsa -t root@x100.fake.net ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -l root"
alias aptnoproxy='apt -o Acquire::http::proxy=false'
alias gits='git status'
alias gitp='git pull'
alias gitf='git fetch'
alias gitl='git log --oneline'
alias gitd='git diff'
alias gitdd='git diff @~..@'
alias gitca='git commit -a'
alias gitco='git checkout'
alias gitcom='git checkout master'
alias gitrom='git rebase origin/master'
alias gitr2='git rebase -i @~2'
alias gitr3='git rebase -i @~3'
alias gitr4='git rebase -i @~4'
alias git2='git commit -a && git rebase -i HEAD~2 && git push -f'
alias giturl='git config --get remote.origin.url'
alias mygit='NO_PROXY=* git \
    -c http.proxy= \
    -c https.proxy= \
    -c core.gitproxy= \
    -c core.editor=vim \
    -c user.email=dimitrii@alexeieff.org \
    -c user.name="Dimitrii Alexeieff" \
    -c commit.template='
alias pip3proxy='sudo python3.8 -m pip --proxy=127.0.0.1:3128'
alias grepr='grep -r \
    --exclude-dir=.mypy_cache \
    --exclude-dir=__pycache__ \
    --exclude-dir=.git \
    --exclude-dir=.tox'

if which most >/dev/null 2>&1
then
   export MANPAGER="most"
fi

fatal() { echo -e "\e[31m$@\e[0m"; exit 1; }
error() { echo -e "\e[31m$@\e[0m"; }
ok()    { echo -e "\e[32m$@\e[0m"; }
warn()  { echo -e "\e[33m$@\e[0m"; }
msg()   { echo -e "\e[36m$@\e[0m"; }

git_rm_branch() {
    [[ -z $1 ]] && {
        echo "Branch name missed"
        return
    }
    git branch -D $1
    git push origin --delete $1
}


# TODO
# find . -type f \( -name "*.sh" -o -name "*.txt" \)
# find . -type f \( -name "*.fb2" -o -name "*.epub" -name "*.mobi" \) -exec cp "{}" $r/antique \;

# yandex_pull() {
#     rm -f /tmp/y.zip
#     zip -q -1 -r /tmp/y.zip /mnt/d/yandex
#     yandex-disk sync --read-only --overwrite
# }

# yandex_push() {
#     rm -f /tmp/y.zip
#     zip -q -1 -r /tmp/y.zip /mnt/d/yandex
#     yandex-disk sync
# }

mrproper() {
    find . -type f -size -52k -exec rm -f {} +
    find . -type f -name "*.js" -exec rm -f {} +
}

prettyjson ()
{
    local tmp=$(mktemp);
    local path=${1:-.};
    path=$(readlink -f $path);
    [[ -f $path ]] && {
        python3 -m json.tool $path > $tmp;
        mv $tmp $path;
        return
    };
    [[ -d $path ]] && {
        for f in ${path}/*.json;
        do
            python3 -m json.tool $f > $tmp;
            mv $tmp $f;
        done
    }
}

